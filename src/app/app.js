// The basics
import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { withRouter } from "react-router";

// Action creators and helpers
import { establishCurrentUser } from "./actions/auth";
import { isServer } from "../store";

import { Header, Footer } from "./components";
import Routes from "./routes";
import ReduxToastr from "react-redux-toastr";
import "react-redux-toastr/lib/css/react-redux-toastr.min.css";

import "./app.css";

class App extends Component {
  componentWillMount() {
    if (!isServer) {
      this.props.establishCurrentUser();
    }
  }
  componentDidUpdate() {
    window.scrollTo(0, 0);
  }
  isAdmin() {
    return (
      this.props.location.pathname.split("/").indexOf("admin") >= 0 ||
      this.props.location.pathname.split("/").indexOf("admin-login") >= 0
    );
  }
  renderHeader() {
    let isAdmin = this.isAdmin();
    if (!isAdmin) {
      return (
        <Header
          isAuthenticated={this.props.isAuthenticated}
          current={this.props.location.pathname}
          history={this.props.history}
        />
      );
    }
  }

  renderFooter() {
    let isAdmin = this.isAdmin();
    if (!isAdmin) {
      return <Footer />;
    }
  }

  render() {
    return (
      <div id="app">
        {this.renderHeader()}
        <div>
          <Routes />
          <ReduxToastr
            timeOut={4000}
            newestOnTop={false}
            preventDuplicates
            position="top-right"
            transitionIn="fadeIn"
            transitionOut="fadeOut"
            closeOnToastrClick
          />
        </div>
        {this.renderFooter()}
      </div>
    );
  }
}

const mapStateToProps = state => ({
  isAuthenticated: state.auth.isAuthenticated
});

const mapDispatchToProps = dispatch =>
  bindActionCreators({ establishCurrentUser }, dispatch);

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(App)
);
