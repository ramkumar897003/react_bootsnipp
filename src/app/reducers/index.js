import { combineReducers } from "redux";
import { reducer as toastrReducer } from "react-redux-toastr";

import auth from "./auth";
import profile from "./profile";
import listing from "./listing";
import adminAuth from "./adminAuth";

export default combineReducers({
  toastr: toastrReducer,
  listing,
  adminAuth,
  auth,
  profile
});
