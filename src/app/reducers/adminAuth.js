import types from "../types";

const initialState = {
  adminScripts: {},
  category: {}
};

export default (state = initialState, action) => {
  switch (action.type) {
    case types.SAVE_ADMIN_SCRIPTS:
      return {
        ...state,
        adminScripts: action.payload
      };
    case types.SAVE_NEW_SCRIPTS:
      let updatedScripts = state.adminScripts;
      updatedScripts.scripts.push(action.payload);
      return {
        ...state,
        adminScripts: updatedScripts
      };
    case types.UPDATE_SCRIPT:
      let allScripts = state.adminScripts;
      let scriptIndex = allScripts.scripts.findIndex(
        item => item._id == action.payload._id
      );
      allScripts.scripts[scriptIndex] = action.payload;
      return {
        ...state,
        adminScripts: allScripts
      };

    case types.SAVE_CATEGORY:
      return {
        ...state,
        category: action.payload
      };

    case types.SAVE_NEW_CATEGORY:
      let updatedCategory = state.category;
      updatedCategory.categories.push(action.payload);
      return {
        ...state,
        category: updatedCategory
      };
    case types.UPDATE_CATEGORY:
      let category = state.category;
      let index = category.categories.findIndex(
        item => item._id == action.payload._id
      );
      category.categories[index] = action.payload;
      return {
        ...state,
        category: category
      };
    default:
      return state;
  }
};
