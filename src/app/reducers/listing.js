import types from "../types";

const initialState = {
  componentList: {},
  componentDetail: {},
  loading: false,
  totalPages: 0,
  relatedScripts: [],
  categories: [],
  search: ""
};

export default (state = initialState, action) => {
  switch (action.type) {
    case types.GET_SCRIPTS:
      return {
        ...state,
        componentList: action.payload
      };
    case types.GET_SCRIPT_DETAIL:
      return {
        ...state,
        componentDetail: action.payload
      };
    case types.SET_LOADING:
      return {
        ...state,
        loading: action.bool
      };
    case types.SET_PAGES:
      return {
        ...state,
        totalPages: action.number
      };
    case types.SET_RELATED_SCRIPT:
      return {
        ...state,
        relatedScripts: action.payload
      };
    case types.GET_CATEGORIES:
      return {
        ...state,
        categories: action.payload
      };
    case types.SET_SEARCH:
      return {
        ...state,
        search: action.payload
      };

    default:
      return state;
  }
};
