import Cookies from "js-cookie";
import types from "../types";
import { loginAPI } from "../api/auth";

export const setCurrentUser = user => dispatch =>
  new Promise(resolve => {
    dispatch({
      type: types.SET_CURRENT_USER,
      user
    });

    Cookies.set("mywebsite", user);

    dispatch({
      type: types.AUTHENTICATE,
      authenticated: true
    });

    resolve(user);
  });

export const establishCurrentUser = () => dispatch =>
  new Promise(resolve => {
    let userFromCookie = Cookies.getJSON("mywebsite");

    if (userFromCookie) {
      dispatch(setCurrentUser(userFromCookie));
      resolve(userFromCookie);
    } else {
      resolve({});
    }
  });

export const loginUser = (email, password) => dispatch =>
  new Promise((resolve, reject) => {
    const user = {
      email,
      password,
      name: "Awesome User"
    };

    dispatch(setCurrentUser(user));
    resolve(user);
  });

export const logoutUser = () => dispatch =>
  new Promise(resolve => {
    dispatch({
      type: types.AUTHENTICATE,
      authenticated: false
    });

    dispatch({
      type: types.SET_CURRENT_USER,
      user: {}
    });

    Cookies.remove("mywebsite");
    resolve({});
  });

export const saveUserAuth = data => {
  console.log(data, "data");
  return {
    type: types.SAVE_USER_AUTH,
    payload: data
  };
};

export const initialUserDataLoad = () => dispatch => {
  let UserData = localStorage.getItem("UserData");
  if (UserData) {
    UserData = JSON.parse(UserData);
  }
  dispatch(saveUserAuth(UserData));
};

export const logout = () => dispatch => {
  localStorage.removeItem("UserData");
  localStorage.removeItem("User_token");
  dispatch(saveUserAuth({}));
};

export const togglefavScript = id => {
  return {
    type: types.SAVE_USER_SCRIPT,
    payload: id
  };
};

export const addFavScripts = data => {
  return {
    type: types.SAVE_USER_SCRIPT_IDS,
    payload: data
  };
};
