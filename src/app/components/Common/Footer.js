import React, { Component } from "react";
import { Link } from "react-router-dom";

export default class Footer extends Component {
  render() {
    return (
      <React.Fragment>
        <footer className="site-footer text-left">
          <div className="container">
            <div className="row">
              <div className="col-lg-4">
                <div className="mb-5">
                  <h3 className="footer-heading mb-4">About Graphicskart</h3>
                  <p>
                    Graphicskart is an UI gallery for Front-end developers,
                    anybody who is using latest web and mobile technologies will
                    find this website essential in their craft. Graphicskart
                    fasten the development work by providing the ready made easy
                    to use UI components.
                  </p>
                </div>
              </div>
              <div className="col-lg-1" />
              <div className="col-lg-4 mb-5 mb-lg-0">
                <div className="row mb-5">
                  <div className="col-md-12">
                    <h3 className="footer-heading mb-4">Navigations</h3>
                  </div>
                  <div className="col-md-6 col-lg-6">
                    <ul className="list-unstyled">
                      <li>
                        <Link to={"/"}>Home</Link>
                      </li>
                      <li>
                        <Link to={"/contact"}>Contact Us</Link>
                      </li>
                      {/*<li>
                        <Link to={"/about"}>About</Link>
                      </li>*/}
                      <li>
                        <Link to={"/privacy"}>Privacy</Link>
                      </li>
                      {/*<li>
                        <Link to={"/term"}>Terms and Conditions</Link>
                      </li>*/}
                    </ul>
                  </div>
                  {/*<div className="col-md-6 col-lg-6">
                    <ul className="list-unstyled">
                      <li>
                        <a href="#">About Us</a>
                      </li>
                      <li>
                        <a href="#">Privacy Policy</a>
                      </li>
                      <li>
                        <a href="#">Contact Us</a>
                      </li>
                      <li>
                        <a href="#">Terms</a>
                      </li>
                    </ul>
                  </div>*/}
                </div>
              </div>
              <div className="col-lg-3 mb-5 mb-lg-0">
                <h3 className="footer-heading mb-4">Follow Us</h3>
                <div>
                  <a href="#" className="pl-0 pr-3">
                    <span className="icon-facebook" />
                  </a>
                  <a href="#" className="pl-3 pr-3">
                    <span className="icon-twitter" />
                  </a>
                  <a href="#" className="pl-3 pr-3">
                    <span className="icon-instagram" />
                  </a>
                  {/*<a href="#" className="pl-3 pr-3">
                    <span className="icon-linkedin" />
                </a>*/}
                </div>
              </div>
            </div>
          </div>
        </footer>
      </React.Fragment>
    );
  }
}
