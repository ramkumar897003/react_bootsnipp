import React from "react";

export default props => {
  return (
    <div className="site-blocks-cover inner-page-cover overlay headingBanner">
      <div className="container">
        <div className="row align-items-center justify-content-flex-start text-left ">
          <div className="col-md-10">
            <h1 className="mb-2">{props.title}</h1>
          </div>
        </div>
      </div>
    </div>
  );
};
