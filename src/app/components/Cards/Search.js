import React, { Component } from "react";
import { connect } from "react-redux";
import { getList } from "../../api/listing";
import queryString from "query-string";
import { setSearchData } from "../../actions/listing";

class Search extends Component {
  constructor(props) {
    super(props);
    this.state = {
      search: ""
    };
  }
  search(event) {
    this.setState({ search: event.target.value });
  }
  onKeyPress(event) {
    let { search } = this.state;
    if (event.key == "Enter") {
      this.props.dispatch(setSearchData(search));
      this.props.history.push("/search?q=" + search);
    }
  }
  render() {
    return (
      <div className="searchBar">
        <input
          type="text"
          className="form-control"
          placeholder="Search"
          onChange={this.search.bind(this)}
          onKeyPress={this.onKeyPress.bind(this)}
        />
      </div>
    );
  }
}

const mapStateToProps = state => ({});

export default connect(state => ({}, mapDispatch))(Search);

const mapDispatch = dispatch => {
  const allActionProps = Object.assign({}, dispatch);
  return allActionProps;
};
