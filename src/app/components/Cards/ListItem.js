import React, { Component } from "react";
import ProgressiveImage from "react-progressive-image-loading";
import { connect } from "react-redux";

class ListItem extends Component {
  goToDetailPage(id) {
    this.props.history.push("/detailPage/" + id);
  }

  renderFavBadge(id) {
    let { auth } = this.props;
    if (auth.userScriptList.indexOf(id) != -1) {
      return (
        <div className="favBadge">
          <i className="fa fa-crown" />
        </div>
      );
    }
  }

  render() {
    return (
      <div className="col-md-6 col-lg-4 mb-4">
        <a
          onClick={this.goToDetailPage.bind(this, this.props.data.slug)}
          className="prop-entry d-block"
        >
          {this.renderFavBadge(this.props.data._id)}
          <figure>
            <img src={this.props.data.image} style={{ height: 350 }} />
          </figure>
          <div className="prop-text">
            <div className="inner text-right">
              <span className="price rounded">{this.props.data.title}</span>
              <h3 className="title">Version: {this.props.data.version}</h3>
              {/*<p className="location">Los Angeles, CA 90005</p>*/}
            </div>
            <div className="prop-more-info">
              <div className="inner d-flex">
                <div className="col">
                  <span>Area:</span>
                  <strong>
                    240m
                    <sup>2</sup>
                  </strong>
                </div>
                <div className="col">
                  <span>Beds:</span>
                  <strong>2</strong>
                </div>
                <div className="col">
                  <span>Baths:</span>
                  <strong>2</strong>
                </div>
                <div className="col">
                  <span>Garages:</span>
                  <strong>1</strong>
                </div>
              </div>
            </div>
          </div>
        </a>
      </div>
    );
  }
}

export default connect(state => ({
  auth: state.auth
}))(ListItem);
