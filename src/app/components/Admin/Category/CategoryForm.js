import React, { Component } from "react";
import { connect } from "react-redux";
import { Modal, Button } from "react-bootstrap";
import InputText from "../../Form/InputText";
import SelectField from "../../Form/SelectField";
import ImageUpload from "../../Form/ImageUpload";
import { AddCategory, UpdateCategory } from "../../../api/adminAuth";

class CategoryForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      newCategory: {
        title: "",
        description: ""
      }
    };
  }

  componentWillReceiveProps(nextProps) {
    if (
      this.props.editFormData != nextProps.editFormData &&
      nextProps.editFormData
    ) {
      this.setState({ newCategory: nextProps.editFormData });
    } else {
      this.resetData();
    }
  }

  onChange(key, event) {
    let { newCategory } = this.state;
    newCategory[key] = event.target.value;
    this.setState({ newCategory });
  }

  resetData() {
    let newCategory = {
      title: "",
      description: ""
    };
    this.setState({ newCategory });
  }

  saveCategory() {
    let { newCategory } = this.state;
    if (newCategory._id) {
      this.props.dispatch(UpdateCategory(newCategory)).then(res => {
        this.resetData();
        this.props.onClose();
      });
    } else {
      this.props.dispatch(AddCategory(newCategory)).then(res => {
        this.resetData();
        this.props.onClose();
      });
    }
  }

  render() {
    let { newCategory } = this.state;
    return (
      <Modal
        show={this.props.isOpen}
        onHide={this.props.onClose.bind(this)}
        className="adminModal"
      >
        <Modal.Header closeButton>
          <Modal.Title>
            {newCategory._id ? "Update Category" : "Add New Category"}
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div className="box box-warning">
            <div className="box-body">
              <form role="form">
                <div className="form-group">
                  <InputText
                    type="text"
                    placeholder="Enter Title"
                    value={newCategory.title}
                    onChange={this.onChange.bind(this, "title")}
                  />
                </div>

                <div className="form-group">
                  <textarea
                    className="form-control"
                    rows="3"
                    placeholder="Enter Description"
                    value={newCategory.description}
                    onChange={this.onChange.bind(this, "description")}
                  />
                </div>
              </form>
            </div>
          </div>
        </Modal.Body>

        <Modal.Footer>
          <button
            type="button"
            className="btn btn-default pull-left"
            data-dismiss="modal"
          >
            Close
          </button>
          <button
            type="button"
            className="btn btn-primary"
            onClick={this.saveCategory.bind(this)}
          >
            {newCategory._id ? "Update" : "Save"}
          </button>
        </Modal.Footer>
      </Modal>
    );
  }
}

const mapStateToProps = state => ({});

export default connect(state => ({}, mapDispatch))(CategoryForm);

const mapDispatch = dispatch => {
  const allActionProps = Object.assign({}, dispatch);
  return allActionProps;
};
