import React, { Component } from "react";
import { connect } from "react-redux";
import { Modal, Button } from "react-bootstrap";
import InputText from "../../Form/InputText";
import SelectField from "../../Form/SelectField";
import ImageUpload from "../../Form/ImageUpload";
import { IsValidForm } from "../../Common/validation";
import Chips, { Chip } from "react-chips-16";
import { AddScript, UpdateScript } from "../../../api/adminAuth";

const suggetions = [
  "Bootstrap",
  "React",
  "HTML",
  "Signup",
  "Modal",
  "Login",
  "Form",
  "VUE",
  "JavaScript",
  "Multistep",
  "Auth",
  "Data Table",
  "Table",
  "Register",
  "Animated",
  "Animation",
  "Contact",
  "Landing Page",
  "Infographics",
  "Dialogs",
  "Alert",
  "Popup",
  "Menu",
  "Navbar",
  "Navigation",
  "Profile",
  "Cards",
  "Shopping Cart"
];

class ScriptsForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      newScript: {
        title: "",
        url: "",
        downloadLink: "",
        category: "",
        tags: [],
        version: "",
        description: "",
        image: ""
      },
      errors: {},
      errormsg: ""
    };
  }

  componentWillReceiveProps(nextProps) {
    if (
      this.props.editFormData != nextProps.editFormData &&
      nextProps.editFormData
    ) {
      this.setState({ newScript: nextProps.editFormData });
    } else {
      this.resetData();
    }
  }

  resetData() {
    let newScript = {
      title: "",
      url: "",
      downloadLink: "",
      category: "",
      tags: [],
      version: "",
      description: "",
      image: ""
    };
    this.setState({ newScript });
  }

  onChange(key, event) {
    let { newScript } = this.state;
    if (event.target) {
      newScript[key] = event.target.value;
    } else {
      newScript[key] = event;
    }

    this.setState({ newScript, errormsg: "" });
  }

  onImageUpload(file) {
    let { newScript } = this.state;
    newScript["image"] = file;
    this.setState({ newScript });
  }

  saveScript() {
    let { newScript } = this.state;
    let fields = [
      "title",
      "url",
      "downloadLink",
      "category",
      "tags",
      "version",
      "description",
      "image"
    ];
    let formValidation = IsValidForm(fields, this.state.newScript);
    this.setState({ errors: formValidation.errors, errormsg: "" });
    if (formValidation.validate) {
      this.setState({ loading: true });
      if (newScript._id) {
        this.props.dispatch(UpdateScript(newScript)).then(res => {
          this.renderValidate(res);
          this.resetData();
          this.props.onClose();
        });
      } else {
        this.props.dispatch(AddScript(newScript)).then(res => {
          this.renderValidate(res);
          this.resetData();
          this.props.onClose();
        });
      }
    }
  }

  renderValidate(res) {
    if (res.errors) {
      this.setState({ errors: res.errors });
    } else {
      this.props.onClose();
    }
  }

  showError(key) {
    let errors = this.state.errors;
    if (errors[key] && errors[key].length) {
      return true;
    }
    return false;
  }
  getError(key) {
    let errors = this.state.errors;
    if (errors[key] && errors[key].length) {
      return typeof errors[key] === "object"
        ? errors[key].join(",")
        : errors[key];
    }
    return false;
  }

  render() {
    let { newScript } = this.state;
    let { category } = this.props.adminAuth;
    return (
      <Modal
        show={this.props.isOpen}
        onHide={this.props.onClose.bind(this)}
        className="adminModal"
      >
        <Modal.Header closeButton>
          <Modal.Title>
            {newScript._id ? "Update Script" : "Add New Script"}
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div className="box box-warning">
            <div className="box-body">
              <form role="form">
                {/*<div className="form-group">
                  <ImageUpload onImageUpload={this.onImageUpload.bind(this)} />
                </div>*/}
                <div className="form-group">
                  <InputText
                    type="text"
                    placeholder="Enter Image URL"
                    value={newScript.image}
                    onChange={this.onChange.bind(this, "image")}
                  />
                  {!!this.showError("image") ? (
                    <p className="error-message">{this.getError("image")} </p>
                  ) : null}
                </div>
                <div className="form-group">
                  <InputText
                    type="text"
                    placeholder="Enter Title"
                    value={newScript.title}
                    onChange={this.onChange.bind(this, "title")}
                  />
                  {!!this.showError("title") ? (
                    <p className="error-message">{this.getError("title")} </p>
                  ) : null}
                </div>
                <div className="form-group">
                  <SelectField
                    placeholder="Enter Category"
                    value={newScript.category}
                    valueKey={"_id"}
                    dataKey={"title"}
                    data={category && category.categories}
                  />
                  {!!this.showError("category") ? (
                    <p className="error-message">
                      {this.getError("category")}{" "}
                    </p>
                  ) : null}
                </div>
                <div className="form-group">
                  <InputText
                    type="text"
                    placeholder="Enter URL"
                    value={newScript.url}
                    onChange={this.onChange.bind(this, "url")}
                  />
                  {!!this.showError("url") ? (
                    <p className="error-message">{this.getError("url")} </p>
                  ) : null}
                </div>
                <div className="form-group">
                  <InputText
                    type="text"
                    placeholder="Enter Download Link"
                    value={newScript.downloadLink}
                    onChange={this.onChange.bind(this, "downloadLink")}
                  />
                  {!!this.showError("downloadLink") ? (
                    <p className="error-message">
                      {this.getError("downloadLink")}{" "}
                    </p>
                  ) : null}
                </div>
                <div className="form-group">
                  <div className="chipsCon">
                    <Chips
                      value={newScript.tags}
                      onChange={this.onChange.bind(this, "tags")}
                      suggestions={suggetions}
                      placeholder="Enter Tags"
                    />
                    {!!this.showError("tags") ? (
                      <p className="error-message">{this.getError("tags")} </p>
                    ) : null}
                  </div>
                </div>
                <div className="form-group">
                  <InputText
                    type="text"
                    placeholder="Enter Version"
                    value={newScript.version}
                    onChange={this.onChange.bind(this, "version")}
                  />
                  {!!this.showError("version") ? (
                    <p className="error-message">{this.getError("version")} </p>
                  ) : null}
                </div>

                <div className="form-group">
                  <textarea
                    className="form-control"
                    rows="3"
                    placeholder="Enter Description"
                    value={newScript.description}
                    onChange={this.onChange.bind(this, "description")}
                  />
                  {!!this.showError("description") ? (
                    <p className="error-message">
                      {this.getError("description")}{" "}
                    </p>
                  ) : null}
                </div>
              </form>
            </div>
          </div>
        </Modal.Body>

        <Modal.Footer>
          <button
            type="button"
            className="btn btn-default pull-left"
            data-dismiss="modal"
          >
            Close
          </button>
          <button
            type="button"
            className="btn btn-primary"
            onClick={this.saveScript.bind(this)}
          >
            {newScript._id ? "Update" : "Save"}
          </button>
        </Modal.Footer>
      </Modal>
    );
  }
}

const mapStateToProps = state => ({});

export default connect(state => ({}, mapDispatch))(ScriptsForm);

const mapDispatch = dispatch => {
  const allActionProps = Object.assign({}, dispatch);
  return allActionProps;
};
