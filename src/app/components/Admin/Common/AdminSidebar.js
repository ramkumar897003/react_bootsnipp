import React, { Component } from "react";
import { connect } from "react-redux";
import { Sidebar } from "react-adminlte-dash";

class AdminSidebar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      links: [
        {
          title: "All Scripts",
          link: "/admin",
          icon: { className: "fa fa-th" }
        },
        {
          title: "Category",
          link: "/admin/category",
          icon: { className: "fa fa-files-o" }
        }
      ]
    };
  }
  goToRoute(url) {
    this.props.history.push(url);
  }

  render() {
    return (
      <Sidebar.Menu
        header="NAVIGATION"
        key="1"
        fixed={true}
        sidebarMini={true}
        sidebarCollapse={true}
      >
        {this.state.links.map((item, index) => {
          return (
            <Sidebar.Menu.Item
              key={index}
              title={item.title}
              onClick={this.goToRoute.bind(this, item.link)}
              icon={item.icon}
            />
          );
        })}
      </Sidebar.Menu>
    );
  }
}

const mapStateToProps = state => ({});

export default connect(state => ({}, mapDispatch))(AdminSidebar);

const mapDispatch = dispatch => {
  const allActionProps = Object.assign({}, dispatch);
  return allActionProps;
};
