import React, { Component } from "react";
import { connect } from "react-redux";
import { Modal, Button } from "react-bootstrap";
import InputText from "../Form/InputText";
import SelectField from "../Form/SelectField";
import ImageUpload from "../Form/ImageUpload";
import { loginAPI, getUserScriptsIds } from "../../api/auth";

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      login: {
        email: "",
        password: ""
      },
      loading: false,
      errorMsg: ""
    };
  }

  onChange(key, event) {
    let { login } = this.state;
    login[key] = event.target.value;
    this.setState({ login, errorMsg: "" });
  }

  resetData() {
    let login = {
      email: "",
      password: ""
    };
    this.setState({ login });
  }

  loginFunction(e) {
    e.preventDefault();
    let { login } = this.state;
    this.setState({ loading: true });
    this.props.dispatch(loginAPI(login)).then(res => {
      console.log(res, "res");
      if (res.errors) {
        this.setState({ errorMsg: res.errors });
      } else {
        this.resetData();
        this.onClose();
        this.props.dispatch(getUserScriptsIds());
        this.props.history.push("/myscripts");
      }
      this.setState({ loading: false });
    });
  }

  onClose() {
    this.setState({ errorMsg: "" });
    this.resetData();
    this.props.onClose();
  }

  renderErrorMsg() {
    let { errorMsg } = this.state;
    if (typeof errorMsg == "string") {
      return errorMsg;
    } else {
      return "Invalid email or password";
    }
  }

  isDisable() {
    let { loading, login } = this.state;
    if (!login.email || !login.password || loading) {
      return true;
    }
  }

  render() {
    let { login } = this.state;
    return (
      <Modal
        show={this.props.isOpen}
        onHide={this.onClose.bind(this)}
        className="signup login"
      >
        <Modal.Body>
          <form role="form" onSubmit={this.loginFunction.bind(this)}>
            <div className="loginAvatar">
              G<span>K</span>
            </div>
            <div className="box box-warning">
              <div className="box-body">
                <div className="form-group">
                  <InputText
                    type="email"
                    placeholder="Email"
                    value={login.email}
                    onChange={this.onChange.bind(this, "email")}
                  />
                </div>
                <div className="form-group">
                  <InputText
                    type="password"
                    placeholder="Password"
                    value={login.password}
                    onChange={this.onChange.bind(this, "password")}
                  />
                </div>
              </div>
            </div>
            <div className="text-center">
              <p className="errormsg">{this.renderErrorMsg()}</p>
            </div>
            <div className="text-right">
              <button
                type="submit"
                className="btn btn-primary"
                disabled={this.isDisable()}
              >
                Login
              </button>
            </div>
          </form>
        </Modal.Body>
      </Modal>
    );
  }
}

const mapStateToProps = state => ({});

export default connect(state => ({}, mapDispatch))(Login);

const mapDispatch = dispatch => {
  const allActionProps = Object.assign({}, dispatch);
  return allActionProps;
};
