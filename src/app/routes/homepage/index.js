import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import Page from "../../components/page";
import ListItem from "../../components/Cards/ListItem";
import queryString from "query-string";
import { getList, getUserScripts } from "../../api/listing";
import ReactPagination from "../../components/Cards/Pagination";
import ContentLoader, { BulletList, Instagram } from "react-content-loader";

class Homepage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentPage: 1,
      loader: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]
    };
  }

  componentDidMount() {
    this.getList(this.state.currentPage, "");
  }

  componentWillReceiveProps(nextProps) {
    if (
      this.props.location.search !== nextProps.location.search ||
      (this.props.location.pathname !== nextProps.location.pathname &&
        (nextProps.location.pathname == "/" ||
          nextProps.location.pathname == "/myscripts"))
    ) {
      this.setState({ currentPage: 1 });
      let searchQuery =
        nextProps.location.pathname == "/" ? "" : nextProps.listing.search;
      this.getList(1, searchQuery, nextProps);
    }
  }

  getValueFromQuery(key, props) {
    if (key == "tag" && props.location.pathname == "/") {
      return "";
    }
    if (props.location.search) {
      let data = queryString.parse(props.location.search);
      return data[key];
    }
  }

  changePage(page) {
    let props = this.props;
    let category = this.getValueFromQuery("category", props) || "";
    let tag = this.getValueFromQuery("tag", props) || "";
    let search = this.getValueFromQuery("q", props) || "";
    let pathname = props.history.location.pathname;
    this.props.history.push({
      pathname: pathname,
      search: `?page=${page}&category=${category}&tag=${tag}&q=${search}`
    });
  }

  getList(page, keyword, props) {
    if (!props) {
      props = this.props;
    }
    let category = this.getValueFromQuery("category", props);
    let tag = this.getValueFromQuery("tag", props);
    let search = this.getValueFromQuery("q", props);
    let pageNo = parseInt(this.getValueFromQuery("page", props)) || 1;
    this.setState({ currentPage: pageNo });
    if (props.history.location.pathname == "/myscripts") {
      this.props.dispatch(getUserScripts(pageNo, search, category, tag));
    } else {
      this.props.dispatch(getList(pageNo, search, category, tag));
    }
  }

  renderLoader() {
    let { listing } = this.props;
    if (listing.loading) {
      return (
        <div className="row">
          {this.state.loader.map((item, index) => {
            return (
              <div
                className="col-md-6 col-lg-4 mb-4"
                style={{ maxHeight: 350, overflow: "hidden" }}
                key={index}
              >
                <Instagram />
              </div>
            );
          })}
        </div>
      );
    }
  }

  renderList() {
    let { listing } = this.props;
    if (!listing.loading && listing.componentList.length) {
      return listing.componentList.map((item, key) => {
        return (
          <ListItem
            data={item.script_id ? item.script_id : item}
            key={key}
            history={this.props.history}
          />
        );
      });
    }
  }

  renderNoData() {
    let { listing } = this.props;
    if (!listing.loading && !listing.componentList.length) {
      return (
        <p style={{ display: "block", textAlign: "center", width: "100%" }}>
          No result found
        </p>
      );
    }
  }

  renderPagination() {
    let { listing } = this.props;
    if (listing.totalPages) {
      return (
        <ReactPagination
          totalPages={listing.totalPages}
          currentPage={this.state.currentPage}
          onChange={this.changePage.bind(this)}
        />
      );
    }
  }

  renderTitle() {
    if (this.props.history.location.pathname == "/myscripts") {
      return <span>My Favorite Components</span>;
    } else {
      return <span>New Components for You</span>;
    }
  }

  render() {
    let { listing } = this.props;
    return (
      <Page id="homepage">
        <div className="text-left">
          <div className="site-section site-section-sm bg-light">
            <div className="container">
              <div className="row mb-5">
                <div className="col-12">
                  <div className="site-section-title paddingTop30">
                    <h2>{this.renderTitle()}</h2>
                  </div>
                </div>
              </div>
              {this.renderLoader()}
              <div className="row mb-5">
                {this.renderList()}
                {this.renderNoData()}
              </div>
              <div className="row">
                <div className="col-md-12 text-center mb-5">
                  {this.renderPagination()}
                </div>
              </div>
            </div>
          </div>
        </div>
      </Page>
    );
  }
}

const mapStateToProps = state => ({});

export default connect(state => ({}, mapDispatch))(Homepage);

const mapDispatch = dispatch => {
  const allActionProps = Object.assign({}, dispatch);
  return allActionProps;
};
