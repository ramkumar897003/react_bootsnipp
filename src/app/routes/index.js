import React from "react";
import { Route, Switch, Redirect } from "react-router-dom";
import AuthenticatedRoute from "../components/authenticated-route";
import UnauthenticatedRoute from "../components/unauthenticated-route";
import Loadable from "react-loadable";

import NotFound from "./not-found";

const Homepage = Loadable({
  loader: () => import(/* webpackChunkName: "homepage" */ "./homepage"),
  loading: () => null,
  modules: ["homepage"]
});

const DetailPage = Loadable({
  loader: () => import(/* webpackChunkName: "homepage" */ "./Detail"),
  loading: () => null,
  modules: ["DetailPage"]
});

const About = Loadable({
  loader: () => import(/* webpackChunkName: "about" */ "./about"),
  loading: () => null,
  modules: ["about"]
});

const Dashboard = Loadable({
  loader: () => import(/* webpackChunkName: "dashboard" */ "./dashboard"),
  loading: () => null,
  modules: ["dashboard"]
});

const Login = Loadable({
  loader: () => import(/* webpackChunkName: "login" */ "./login"),
  loading: () => null,
  modules: ["login"]
});

const Logout = Loadable({
  loader: () => import(/* webpackChunkName: "logout" */ "./logout"),
  loading: () => null,
  modules: ["logout"]
});

const Profile = Loadable({
  loader: () => import(/* webpackChunkName: "profile" */ "./profile"),
  loading: () => null,
  modules: ["profile"]
});

const Contact = Loadable({
  loader: () => import(/* webpackChunkName: "profile" */ "./contact"),
  loading: () => null,
  modules: ["Contact"]
});

const Privacy = Loadable({
  loader: () =>
    import(/* webpackChunkName: "profile" */ "./ContentPages/Privacy"),
  loading: () => null,
  modules: ["Privacy"]
});

const Term = Loadable({
  loader: () => import(/* webpackChunkName: "profile" */ "./ContentPages/Term"),
  loading: () => null,
  modules: ["Term"]
});

const Admin = Loadable({
  loader: () => import(/* webpackChunkName: "profile" */ "./Admin"),
  loading: () => null,
  modules: ["Admin"]
});

const Main = Loadable({
  loader: () => import(/* webpackChunkName: "profile" */ "./Admin/main"),
  loading: () => null,
  modules: ["Main"]
});

const VerifyAccount = Loadable({
  loader: () => import(/* webpackChunkName: "profile" */ "./VerifyAccount"),
  loading: () => null,
  modules: ["VerifyAccount"]
});

const checkAuth = () => {
  const token = localStorage.getItem("Admin_token");
  return !!token;
};

const AuthRoute = ({ component: Component, ...rest }) => (
  <Route
    {...rest}
    render={props =>
      checkAuth() ? (
        <Component {...props} />
      ) : (
        <Redirect to={{ pathname: "/admin-login" }} />
      )
    }
  />
);

const checkUserAuth = () => {
  const token = localStorage.getItem("User_token");
  return !!token;
};

const UserAuthRoute = ({ component: Component, ...rest }) => (
  <Route
    {...rest}
    render={props =>
      checkUserAuth() ? (
        <Component {...props} />
      ) : (
        <Redirect to={{ pathname: "/" }} />
      )
    }
  />
);

export default () => (
  <Switch>
    <Route exact path="/" component={Homepage} />
    <Route exact path="/search" component={Homepage} />
    <Route exact path="/detailPage/:id" component={DetailPage} />

    {/*user route*/}
    <UserAuthRoute exact path="/myscripts" component={Homepage} />

    {/*<Route exact path="/about" component={About} />*/}
    <Route exact path="/contact" component={Contact} />
    <Route exact path="/privacy" component={Privacy} />
    <Route exact path="/term" component={Term} />

    <Route exact path="/verifyaccount/:token" component={VerifyAccount} />

    {/*admin route*/}
    <Route exact path="/admin-login" component={Admin} />
    <AuthRoute path="/admin" component={props => <Main {...props} />} />

    <Route component={NotFound} />
  </Switch>
);
