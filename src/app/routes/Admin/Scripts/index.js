import React, { Component } from "react";
import { connect } from "react-redux";
import Page from "../../../components/page";
import CustomDataTable from "../../../components/CustomDataTable";
import { Button } from "react-bootstrap";
import {
  getAdminScripts,
  deleteAdminScripts,
  getCategory
} from "../../../api/adminAuth";
import ReactPagination from "../../../components/Cards/Pagination";
import ScriptsForm from "../../../components/Admin/Scripts/ScriptsForm";
import swal from "sweetalert";

export function BindName(props) {
  return <span>{props.first_name + " " + props.last_name}</span>;
}

export function BindImage(props) {
  return <img src={props.image} width="200" />;
}

class DeleteButtons extends Component {
  deleteScript() {
    swal({
      title: "Are you sure?",
      text: "Once deleted, you will not be able to recover this script!",
      icon: "warning",
      buttons: true,
      dangerMode: true
    }).then(willDelete => {
      if (willDelete) {
        this.props.confirmDelete(this.props.id);
      }
    });
  }
  render() {
    return (
      <div className="tableBtns">
        <Button
          className="btn btn-primary"
          onClick={this.props.editScript.bind(this, this.props.id)}
        >
          Edit
        </Button>
        <Button
          className="btn btn-default"
          onClick={this.deleteScript.bind(this)}
        >
          Delete
        </Button>
      </div>
    );
  }
}

class Scripts extends Component {
  constructor(props) {
    super(props);
    this.state = {
      filterQuery: "",
      loading: false,
      show: false,
      currentPage: 1,
      editFormData: null
    };
  }

  onChange(key, event) {
    let { newScript } = this.state;
    newScript[key] = event.target.value;
    this.setState({ newScript });
  }

  componentDidMount() {
    this.getScripts(this.state.currentPage);
    this.props.dispatch(getCategory());
  }

  getScripts(page) {
    this.setState({ loading: true, currentPage: page });
    this.props.dispatch(getAdminScripts(page)).then(res => {
      this.setState({ loading: false });
    });
  }

  handleClose() {
    this.setState({ show: false, editFormData: null });
  }

  handleOpen() {
    this.setState({ show: true });
  }

  getData() {
    let { adminScripts } = this.props.adminAuth;
    if (adminScripts && adminScripts.scripts) {
      return adminScripts.scripts;
    }
    return [];
  }

  renderPagination() {
    let { listing } = this.props;
    let { adminScripts } = this.props.adminAuth;
    if (adminScripts.total) {
      return (
        <ReactPagination
          totalPages={adminScripts.total}
          currentPage={this.state.currentPage}
          onChange={this.getScripts.bind(this)}
          perPage={10}
        />
      );
    }
  }

  onConfirm() {}

  confirmDelete(id) {
    this.props.dispatch(deleteAdminScripts(id)).then(res => {
      this.getScripts(this.state.currentPage);
    });
  }

  editScript(id) {
    let { adminScripts } = this.props.adminAuth;
    if (adminScripts) {
      let currentData = adminScripts.scripts.find(item => item._id == id);
      this.setState({ editFormData: currentData });
      this.handleOpen();
    }
  }

  render() {
    let { adminScripts } = this.props.adminAuth;
    let { newScript } = this.state;
    return (
      <Page id="scripts">
        <div className="adminContentCon">
          <div className="topHeader">
            <h4>All Scripts</h4>
            <button
              className="btn btn-primary"
              onClick={this.handleOpen.bind(this)}
            >
              Add New
            </button>
          </div>
          <div className="tableCon">
            <CustomDataTable
              data={this.getData()}
              loading={this.state.loading}
              noPagination={true}
              columns={[
                {
                  header: "Image",
                  component: <BindImage />,
                  component_props: { image: "image" },
                  value: "title",
                  style: { width: "470px" },
                  sortable: true
                },
                {
                  header: "id",
                  value: "_id",
                  sortable: true
                },
                {
                  header: "Title",
                  value: "title",
                  style: { width: "470px" },
                  sortable: true
                },
                {
                  header: "Created At",
                  value: "createdAt",
                  style: { width: "470px" },
                  sortable: true
                },
                {
                  header: "Description",
                  value: "description",
                  style: { width: "470px" },
                  sortable: true
                },
                {
                  header: "Version",
                  value: "version",
                  style: { width: "470px" },
                  sortable: true
                },
                {
                  header: "Views",
                  value: "views",
                  style: { width: "370px" },
                  sortable: true
                },
                {
                  header: "Action",
                  component: (
                    <DeleteButtons
                      confirmDelete={this.confirmDelete.bind(this)}
                      editScript={this.editScript.bind(this)}
                    />
                  ),
                  component_props: { id: "_id" }
                }
              ]}
              filterQuery={this.state.filterQuery}
            />
          </div>
        </div>
        <div className="adminPaginationCOn">
          <div className="text-center">{this.renderPagination()}</div>
        </div>

        <ScriptsForm
          isOpen={this.state.show}
          onClose={this.handleClose.bind(this)}
          onConfirm={this.onConfirm.bind(this)}
          editFormData={this.state.editFormData}
        />
      </Page>
    );
  }
}

const mapStateToProps = state => ({});

export default connect(state => ({}, mapDispatch))(Scripts);

const mapDispatch = dispatch => {
  const allActionProps = Object.assign({}, dispatch);
  return allActionProps;
};
