import React, { Component } from "react";
import { connect } from "react-redux";
import Page from "../../components/page";
import { Dashboard, Sidebar } from "react-adminlte-dash";
import { Route, Switch, BrowserRouter as Router } from "react-router-dom";
import Loadable from "react-loadable";
import AdminHeader from "../../components/Admin/Common/AdminHeader";
import AdminSidebar from "../../components/Admin/Common/AdminSidebar";

const Scripts = Loadable({
  loader: () => import(/* webpackChunkName: "homepage" */ "./Scripts"),
  loading: () => null,
  modules: ["Scripts"]
});

const Category = Loadable({
  loader: () => import(/* webpackChunkName: "homepage" */ "./Category"),
  loading: () => null,
  modules: ["Category"]
});

class Main extends Component {
  render() {
    const { match } = this.props;
    return (
      <div id="admin" className="text-left">
        <Dashboard
          navbarChildren={<AdminHeader />}
          sidebarChildren={<AdminSidebar history={this.props.history} />}
          sidebarMini={true}
          theme="skin-blue"
          fixed={true}
          logoLg={
            <span>
              <b>Graphic</b>
              Kart
            </span>
          }
          logoSm={
            <span>
              <b>G</b>K
            </span>
          }
        >
          <div className="adminContent">
            <Route exact path={`${match.url}`} component={Scripts} />
            <Route exact path={`${match.url}/category`} component={Category} />
          </div>
        </Dashboard>
      </div>
    );
  }
}

const mapStateToProps = state => ({});

export default connect(state => ({}, mapDispatch))(Main);

const mapDispatch = dispatch => {
  const allActionProps = Object.assign({}, dispatch);
  return allActionProps;
};
