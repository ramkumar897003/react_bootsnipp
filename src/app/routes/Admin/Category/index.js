import React, { Component } from "react";
import { connect } from "react-redux";
import Page from "../../../components/page";
import CustomDataTable from "../../../components/CustomDataTable";
import { Button } from "react-bootstrap";
import { getCategory, deleteCategory } from "../../../api/adminAuth";
import ReactPagination from "../../../components/Cards/Pagination";
import CategoryForm from "../../../components/Admin/Category/CategoryForm";
import swal from "sweetalert";

class DeleteButtons extends Component {
  deleteCategory() {
    swal({
      title: "Are you sure?",
      text: "Once deleted, you will not be able to recover this category!",
      icon: "warning",
      buttons: true,
      dangerMode: true
    }).then(willDelete => {
      if (willDelete) {
        this.props.confirmDelete(this.props.id);
      }
    });
  }
  render() {
    return (
      <div className="tableBtns">
        <Button
          className="btn btn-primary"
          onClick={this.props.editCategory.bind(this, this.props.id)}
        >
          Edit
        </Button>
        <Button
          className="btn btn-default"
          onClick={this.deleteCategory.bind(this)}
        >
          Delete
        </Button>
      </div>
    );
  }
}

class Category extends Component {
  constructor(props) {
    super(props);
    this.state = {
      filterQuery: "",
      loading: false,
      show: false,
      currentPage: 1,
      editFormData: null
    };
  }

  componentWillMount() {
    this.getCategory(this.state.currentPage);
  }

  getCategory(page) {
    this.setState({ loading: true, currentPage: page });
    this.props.dispatch(getCategory(page)).then(res => {
      this.setState({ loading: false });
    });
  }

  handleClose() {
    this.setState({ show: false });
    this.setState({ editFormData: null });
  }

  handleOpen() {
    this.setState({ show: true });
  }

  getData() {
    let { category } = this.props.adminAuth;
    if (category && category.categories) {
      return category.categories;
    }
    return [];
  }

  renderPagination() {
    let { listing } = this.props;
    let { category } = this.props.adminAuth;
    if (category.total) {
      return (
        <ReactPagination
          totalPages={category.total}
          currentPage={this.state.currentPage}
          onChange={this.getCategory.bind(this)}
          perPage={10}
        />
      );
    }
  }

  onConfirm() {}

  confirmDelete(id) {
    this.props.dispatch(deleteCategory(id)).then(res => {
      this.getCategory(this.state.currentPage);
    });
  }

  editCategory(id) {
    let { category } = this.props.adminAuth;
    if (category) {
      let currentData = category.categories.find(item => item._id == id);
      this.setState({ editFormData: currentData });
      this.handleOpen();
    }
  }

  render() {
    let { category } = this.props.adminAuth;
    return (
      <Page id="scripts">
        <div className="adminContentCon">
          <div className="topHeader">
            <h4>All Scripts</h4>
            <button
              className="btn btn-primary"
              onClick={this.handleOpen.bind(this)}
            >
              Add New
            </button>
          </div>
          <div className="tableCon">
            <CustomDataTable
              data={this.getData()}
              loading={this.state.loading}
              noPagination={true}
              columns={[
                {
                  header: "id",
                  value: "_id",
                  sortable: true
                },
                {
                  header: "Title",
                  value: "title",
                  style: { width: "470px" },
                  sortable: true
                },
                {
                  header: "Slug",
                  value: "slug",
                  style: { width: "470px" },
                  sortable: true
                },
                {
                  header: "Description",
                  value: "description",
                  style: { width: "470px" },
                  sortable: true
                },
                {
                  header: "Action",
                  component: (
                    <DeleteButtons
                      confirmDelete={this.confirmDelete.bind(this)}
                      editCategory={this.editCategory.bind(this)}
                    />
                  ),
                  component_props: { id: "_id" }
                }
              ]}
              filterQuery={this.state.filterQuery}
            />
          </div>
        </div>
        <div className="adminPaginationCOn">
          <div className="text-center">{this.renderPagination()}</div>
        </div>

        <CategoryForm
          isOpen={this.state.show}
          onClose={this.handleClose.bind(this)}
          onConfirm={this.onConfirm.bind(this)}
          editFormData={this.state.editFormData}
        />
      </Page>
    );
  }
}

const mapStateToProps = state => ({});

export default connect(state => ({}, mapDispatch))(Category);

const mapDispatch = dispatch => {
  const allActionProps = Object.assign({}, dispatch);
  return allActionProps;
};
