import React, { Component } from "react";
import { connect } from "react-redux";
import Page from "../../components/page";
import InputText from "../../components/Form/InputText";
import { IsValidForm } from "../../components/Common/validation";
import { adminLogin } from "../../api/adminAuth";
import {
  Grid,
  Col,
  Row,
  Button,
  Modal,
  FormGroup,
  FormControl
} from "react-bootstrap";

class Admin extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      login: {
        email: "",
        password: ""
      },
      errors: {},
      errormsg: ""
    };
  }

  onInputChange(key, event) {
    let { login } = this.state;
    login[key] = event.target.value;
    this.setState({ login, errormsg: "" });
  }

  login() {
    let fields = ["email", "password"];
    let formValidation = IsValidForm(fields, this.state.login);
    this.setState({ errors: formValidation.errors, errormsg: "" });
    if (formValidation.validate) {
      this.setState({ loading: true });
      this.props
        .dispatch(adminLogin(this.state.login))
        .then(res => {
          this.setState({ loading: false });
          if (res.errors && (res.errors.email || res.errors.password)) {
            this.setState({ errors: res.errors });
          } else if (res.errors) {
            this.setState({ errormsg: res.errors });
          } else {
            this.props.history.push("/admin");
          }
        })
        .catch(function(error) {
          this.setState({ loading: false });
        });
    }
  }
  showError(key) {
    let errors = this.state.errors;
    if (errors[key] && errors[key].length) {
      return true;
    }
    return false;
  }
  getError(key) {
    let errors = this.state.errors;
    if (errors[key] && errors[key].length) {
      return typeof errors[key] === "object"
        ? errors[key].join(",")
        : errors[key];
    }
    return false;
  }

  render() {
    return (
      <Page id="admin">
        <div className="loginCon">
          <div className="adminLogin modalCon hasShadow">
            <div className="roundLogo">
              {/*<img src="/../images/logo-blue.png" />*/}
              GK
            </div>
            <div className="loginForm">
              <FormGroup>
                <InputText
                  iconClassName="fa fa-envelope"
                  type="email"
                  placeholder="Email"
                  value={this.state.login.email}
                  onChange={this.onInputChange.bind(this, "email")}
                />
                {!!this.showError("email") ? (
                  <p className="error-message">{this.getError("email")} </p>
                ) : null}
              </FormGroup>
              <FormGroup>
                <InputText
                  iconClassName="fa fa-eye"
                  type="password"
                  placeholder="Password"
                  value={this.state.login.password}
                  onChange={this.onInputChange.bind(this, "password")}
                />
                {!!this.showError("password") ? (
                  <p className="error-message">{this.getError("password")} </p>
                ) : null}
              </FormGroup>
              {this.state.errormsg ? (
                <p style={{ textAlign: "center", color: "red" }}>
                  {this.state.errormsg}
                </p>
              ) : null}
              <div className="text-right">
                <Button style={{ background: "none" }}>Forgot Password?</Button>
                <Button
                  className="btn btn-primary submitBtn"
                  disabled={this.state.loading}
                  onClick={this.login.bind(this)}
                >
                  {this.state.loading ? (
                    <img src="/assets/img/loading.gif" width="30px" />
                  ) : (
                    "Login"
                  )}
                </Button>
              </div>
            </div>
          </div>
        </div>
      </Page>
    );
  }
}

const mapStateToProps = state => ({});

export default connect(state => ({}, mapDispatch))(Admin);

const mapDispatch = dispatch => {
  const allActionProps = Object.assign({}, dispatch);
  return allActionProps;
};
