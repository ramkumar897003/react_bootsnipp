import React, { Component } from "react";
import { connect } from "react-redux";
import Page from "../../components/page";
import { contactus } from "../../api/auth";
import { IsValidForm, validateField } from "../../components/Common/validation";
import Banner from "../../components/Common/Banner";
import ContactInfo from "../../components/Contact/ContactInfo";
import ContactForm from "../../components/Contact/ContactForm";

class Contact extends Component {
  render() {
    return (
      <Page id="Contact">
        <Banner title="Contact Us" />
        <div className="site-section text-left">
          <div className="container">
            <div className="row">
              {/*<div className="col-lg-4">
                <ContactInfo />
    </div>*/}
              <div className="col-md-12 col-lg-8 mb-5 offset-lg-2">
                <ContactForm />
              </div>
            </div>
          </div>
        </div>
      </Page>
    );
  }
}

const mapStateToProps = state => ({});

export default connect(state => ({}, mapDispatch))(Contact);

const mapDispatch = dispatch => {
  const allActionProps = Object.assign({}, dispatch);
  return allActionProps;
};
