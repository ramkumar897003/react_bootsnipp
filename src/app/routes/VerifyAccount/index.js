import React, { Component } from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { frontloadConnect } from "react-frontload";
import Page from "../../components/page";
import { verifyAccount } from "../../api/auth";

class VerifyAccount extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      errorMsg: ""
    };
  }

  componentDidMount() {
    console.log(this.props.match.params.token, "props");
    this.setState({ loading: true });
    let decodeToken = JSON.parse(window.atob(this.props.match.params.token));
    console.log(decodeToken, "decodeToken");
    this.props.dispatch(verifyAccount(decodeToken.token)).then(res => {
      this.setState({ loading: false });
      console.log(res, "res");
      if (res.errors) {
        this.setState({ errorMsg: res.errors });
      } else {
        this.setState({ errorMsg: "" });
      }
    });
  }

  renderStatus() {
    let { loading, errorMsg } = this.state;
    if (loading) {
      return "loading...";
    } else if (errorMsg) {
      return "Invalid token, or already used";
    } else {
      return "Account confirm successfully, You can login now";
    }
  }
  rednerClass() {
    let { loading, errorMsg } = this.state;
    if (loading) {
      return "icon loading";
    } else if (errorMsg) {
      return "icon fail";
    } else {
      return "icon success";
    }
  }

  render() {
    return (
      <Page id="profile" description={`This is user profile number`}>
        <div className="accountVerifyCon bg-light">
          <div className={this.rednerClass()} />
          <p>{this.renderStatus()}</p>
        </div>
      </Page>
    );
  }
}

const mapStateToProps = state => ({});

export default connect(state => ({}, mapDispatch))(VerifyAccount);

const mapDispatch = dispatch => {
  const allActionProps = Object.assign({}, dispatch);
  return allActionProps;
};
