import React, { Component } from "react";
import { connect } from "react-redux";
import { FacebookProvider, Comments, ShareButton } from "react-facebook";
import Page from "../../components/page";
import ProgressiveImage from "react-progressive-image-loading";
import ListItem from "../../components/Cards/ListItem";
import DetailCard from "../../components/Cards/DetailCard";
import { Modal, Button, Container, Col } from "react-bootstrap";
import { updateDetail, setSearchData } from "../../actions/listing";
import { getDetail, getRelatedScripts } from "../../api/listing";
import { toggleFavScript } from "../../api/auth";
import { toastr } from "react-redux-toastr";

class DetailPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      list: [1, 1, 1, 1, 1, 1],
      show: false,
      loading: false,
      loadingFav: false
    };
  }

  componentDidMount() {
    this.getDetail(this.props.match.params.id);
  }
  componentWillReceiveProps(nextProps) {
    if (this.props.match.params.id != nextProps.match.params.id) {
      this.getDetail(nextProps.match.params.id);
    }
  }

  getDetail(id) {
    this.setState({ loading: true });
    this.props.dispatch(updateDetail({}));
    this.props.dispatch(getRelatedScripts(id));
    this.props.dispatch(getDetail(id)).then(res => {
      this.setState({ loading: false });
    });
  }
  componentWillUnmount() {
    this.props.dispatch(updateDetail({}));
  }

  download(link) {
    window.open(link, "_blank");
  }

  renderHeading() {
    let { relatedScripts } = this.props.listing;
    if (relatedScripts.length) {
      return (
        <h2>
          <span>Related Components</span>
        </h2>
      );
    }
  }

  renderRelatedScripts() {
    let { relatedScripts } = this.props.listing;
    if (relatedScripts.length) {
      return relatedScripts.map((item, key) => {
        return <ListItem data={item} key={key} history={this.props.history} />;
      });
    }
  }

  searchWithTag(item) {
    this.props.dispatch(setSearchData(""));
    this.props.history.push("/search?tag=" + item);
  }

  renderTags(tags) {
    if (tags && tags.length) {
      return (
        <ul className="tags">
          {tags.map((item, index) => {
            return (
              <li key={index} onClick={this.searchWithTag.bind(this, item)}>
                <span>{item}</span>
              </li>
            );
          })}
        </ul>
      );
    }
  }

  renderContent(type) {
    let { auth, listing } = this.props;
    let currentScriptId = listing.componentDetail._id;
    let index = auth.userScriptList.indexOf(currentScriptId);
    if (type == "class") {
      if (index == -1) {
        return "btn btn-default";
      } else {
        return "btn btn-danger";
      }
    } else {
      if (index == -1) {
        return "Add To Favorite";
      } else {
        return "Remove From Favorite";
      }
    }
  }

  renderFavButton() {
    let { loading, loadingFav } = this.state;
    let { auth, listing } = this.props;
    let currentScriptId = listing.componentDetail._id;
    if (!loading && auth.userToken) {
      return (
        <div className="downloadLink">
          <button
            className={this.renderContent("class")}
            disabled={loadingFav}
            onClick={this.toggleFavScript.bind(this, currentScriptId)}
          >
            {this.renderContent("label")}
          </button>
        </div>
      );
    }
  }

  toggleFavScript(id) {
    let data = { script_id: id };
    this.setState({ loadingFav: true });
    this.props.dispatch(toggleFavScript(data)).then(res => {
      if (res.message == "") {
      }
      this.setState({ loadingFav: false });
      toastr.success("SucessFull", res.message);
    });
  }

  render() {
    let { list } = this.state;
    let { componentDetail, relatedScripts } = this.props.listing;
    return (
      <Page
        id="DetailPage"
        title={`${componentDetail.title || "Graphicskart.com"}`}
        description={componentDetail.description}
        image={componentDetail.image}
        url={componentDetail.url}
      >
        <div className="text-left">
          <div className="site-section site-section-sm bg-light">
            <div className="topContainer">
              <div className="container">
                <div className="row">
                  <div className="col-lg-6">
                    <h2 className="h4 text-black">{componentDetail.title}</h2>
                    {this.renderTags(componentDetail.tags)}
                  </div>
                  <div className="col-lg-6 text-right">
                    <button className="btn btn-default">
                      <i className="fa fa-eye" />
                      {componentDetail.views}
                    </button>
                    <a
                      href={componentDetail.url}
                      target="_blank"
                      className="btn btn-default"
                    >
                      View Fullscreen
                    </a>
                    <FacebookProvider appId="350362158934482">
                      <ShareButton
                        href={`http://graphicskart.com/detailPage/${
                          componentDetail.slug
                        }`}
                        style={{ borderRadius: 3 }}
                        className="btn btn-default shareBtn"
                      >
                        <i className="fa fa-share-alt" />
                        Share
                      </ShareButton>
                    </FacebookProvider>
                  </div>
                </div>
              </div>
              <div className="topFrame">
                <iframe
                  src={componentDetail.url}
                  className="componentPreview"
                />
              </div>
            </div>
            <div className="container">
              <div className="row">
                <div className="col-lg-9">
                  <DetailCard detail={componentDetail} />
                </div>
                <div className="col-lg-3 pl-md-5">
                  <div className="downloadLink">
                    <button
                      className="btn btn-primary"
                      onClick={this.download.bind(
                        this,
                        componentDetail.downloadLink
                      )}
                    >
                      Download Zip
                    </button>
                  </div>
                  {this.renderFavButton()}
                </div>
              </div>
              <div className="row paddingTop30 paddingBottom30">
                <div className="col-12">
                  <div className="site-section-title">
                    {this.renderHeading()}
                  </div>
                </div>
              </div>
              <div className="row mb-5">{this.renderRelatedScripts()}</div>
            </div>
            <Container className="comments-section">
              <Col xs={12}>
                <h2>Questions / Comments:</h2>
                <FacebookProvider appId="350362158934482">
                  <Comments href={componentDetail.url} />
                </FacebookProvider>
              </Col>
            </Container>
          </div>
        </div>
      </Page>
    );
  }
}

const mapStateToProps = state => ({});

export default connect(state => ({}, mapDispatch))(DetailPage);

const mapDispatch = dispatch => {
  const allActionProps = Object.assign({}, dispatch);
  return allActionProps;
};
