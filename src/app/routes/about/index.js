import React, { Component } from "react";
import { connect } from "react-redux";
import Page from "../../components/page";
import { contactus } from "../../api/auth";
import { IsValidForm, validateField } from "../../components/Common/validation";
import { toastr } from "react-redux-toastr";
import Banner from "../../components/Common/Banner";
import ContactInfo from "../../components/Contact/ContactInfo";
import ContactForm from "../../components/Contact/ContactForm";

class About extends Component {
  render() {
    return (
      <Page id="about">
        <Banner title="About" />
        <div className="site-section text-left">
          <div className="container">
            <div className="row">
              <div className="col-md-12">
                <div className="site-section-title mb-3">
                  <h2>
                    <span>Our Company</span>
                  </h2>
                </div>
                <p>
                  Lorem ipsum dolor sit amet consectetur adipisicing elit. Natus
                  in cum odio. Lorem ipsum dolor sit amet consectetur
                  adipisicing elit. Natus in cum odio. Lorem ipsum dolor sit
                  amet consectetur adipisicing elit. Natus in cum odio.
                </p>
                <p>
                  Illum repudiandae ratione facere explicabo. Consequatur dolor
                  optio iusto, quos autem voluptate ea? Sunt laudantium fugiat,
                  mollitia voluptate? Modi blanditiis veniam nesciunt architecto
                  odit voluptatum tempore impedit magnam itaque natus!
                </p>
                <p>
                  Lorem ipsum dolor sit amet consectetur adipisicing elit. Natus
                  in cum odio. Lorem ipsum dolor sit amet consectetur
                  adipisicing elit. Natus in cum odio. Lorem ipsum dolor sit
                  amet consectetur adipisicing elit. Natus in cum odio.
                </p>
                <p>
                  Illum repudiandae ratione facere explicabo. Consequatur dolor
                  optio iusto, quos autem voluptate ea? Sunt laudantium fugiat,
                  mollitia voluptate? Modi blanditiis veniam nesciunt architecto
                  odit voluptatum tempore impedit magnam itaque natus!
                </p>
                <p>
                  Lorem ipsum dolor sit amet consectetur adipisicing elit. Natus
                  in cum odio. Lorem ipsum dolor sit amet consectetur
                  adipisicing elit. Natus in cum odio. Lorem ipsum dolor sit
                  amet consectetur adipisicing elit. Natus in cum odio.
                </p>
                <p>
                  Illum repudiandae ratione facere explicabo. Consequatur dolor
                  optio iusto, quos autem voluptate ea? Sunt laudantium fugiat,
                  mollitia voluptate? Modi blanditiis veniam nesciunt architecto
                  odit voluptatum tempore impedit magnam itaque natus!
                </p>
                <p>
                  Lorem ipsum dolor sit amet consectetur adipisicing elit. Natus
                  in cum odio. Lorem ipsum dolor sit amet consectetur
                  adipisicing elit. Natus in cum odio. Lorem ipsum dolor sit
                  amet consectetur adipisicing elit. Natus in cum odio.
                </p>
                <p>
                  Illum repudiandae ratione facere explicabo. Consequatur dolor
                  optio iusto, quos autem voluptate ea? Sunt laudantium fugiat,
                  mollitia voluptate? Modi blanditiis veniam nesciunt architecto
                  odit voluptatum tempore impedit magnam itaque natus!
                </p>
              </div>
            </div>

            <div
              class="row mb-5 justify-content-center"
              style={{ marginTop: 50 }}
            >
              <div class="col-md-12">
                <div class="site-section-title">
                  <h2>
                    <span>Leadership</span>
                  </h2>
                </div>
              </div>
            </div>

            <div className="row">
              <div className="col-md-6 col-lg-4 mb-5 mb-lg-5">
                <div className="team-member">
                  <img
                    src="https://colorlib.com/preview/theme/homespace/images/person_1.jpg"
                    alt="Image"
                    className="img-fluid rounded mb-4"
                  />
                  <div className="text">
                    <h2 className="mb-2 font-weight-light text-black h4">
                      Ram Kumar
                    </h2>
                    <span className="d-block mb-3 text-white-opacity-05">
                      Real Estate Agent
                    </span>
                    <p>
                      Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                      Modi dolorem totam non quis facere blanditiis praesentium
                      est. Totam atque corporis nisi, veniam non. Tempore
                      cupiditate, vitae minus obcaecati provident beatae!
                    </p>
                    <p>
                      <a href="#" className="text-black p-2">
                        <span className="icon-facebook" />
                      </a>
                      <a href="#" className="text-black p-2">
                        <span className="icon-twitter" />
                      </a>
                      <a href="#" className="text-black p-2">
                        <span className="icon-linkedin" />
                      </a>
                    </p>
                  </div>
                </div>
              </div>
              <div className="col-md-6 col-lg-4 mb-5 mb-lg-5">
                <div className="team-member">
                  <img
                    src="https://colorlib.com/preview/theme/homespace/images/person_1.jpg"
                    alt="Image"
                    className="img-fluid rounded mb-4"
                  />
                  <div className="text">
                    <h2 className="mb-2 font-weight-light text-black h4">
                      Rupinderpal Singh
                    </h2>
                    <span className="d-block mb-3 text-white-opacity-05">
                      Real Estate Agent
                    </span>
                    <p>
                      Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                      Modi dolorem totam non quis facere blanditiis praesentium
                      est. Totam atque corporis nisi, veniam non. Tempore
                      cupiditate, vitae minus obcaecati provident beatae!
                    </p>
                    <p>
                      <a href="#" className="text-black p-2">
                        <span className="icon-facebook" />
                      </a>
                      <a href="#" className="text-black p-2">
                        <span className="icon-twitter" />
                      </a>
                      <a href="#" className="text-black p-2">
                        <span className="icon-linkedin" />
                      </a>
                    </p>
                  </div>
                </div>
              </div>
              <div className="col-md-6 col-lg-4 mb-5 mb-lg-5">
                <div className="team-member">
                  <img
                    src="https://colorlib.com/preview/theme/homespace/images/person_1.jpg"
                    alt="Image"
                    className="img-fluid rounded mb-4"
                  />
                  <div className="text">
                    <h2 className="mb-2 font-weight-light text-black h4">
                      Pratham Mehra
                    </h2>
                    <span className="d-block mb-3 text-white-opacity-05">
                      Real Estate Agent
                    </span>
                    <p>
                      Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                      Modi dolorem totam non quis facere blanditiis praesentium
                      est. Totam atque corporis nisi, veniam non. Tempore
                      cupiditate, vitae minus obcaecati provident beatae!
                    </p>
                    <p>
                      <a href="#" className="text-black p-2">
                        <span className="icon-facebook" />
                      </a>
                      <a href="#" className="text-black p-2">
                        <span className="icon-twitter" />
                      </a>
                      <a href="#" className="text-black p-2">
                        <span className="icon-linkedin" />
                      </a>
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </Page>
    );
  }
}

const mapStateToProps = state => ({});

export default connect(state => ({}, mapDispatch))(About);

const mapDispatch = dispatch => {
  const allActionProps = Object.assign({}, dispatch);
  return allActionProps;
};
