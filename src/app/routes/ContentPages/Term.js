import React, { Component } from "react";
import { connect } from "react-redux";
import Page from "../../components/page";
import Banner from "../../components/Common/Banner";

class Term extends Component {
  render() {
    return (
      <Page id="about">
        <Banner title="Term and Conditions" />
        <div className="site-section text-left">
          <div className="container">
            <div className="row">
              <div className="col-md-12">
                {/*<div className="site-section-title mb-3">
									<h2><span>Our Company</span></h2>
								</div>*/}
                <p>
                  Lorem ipsum dolor sit amet consectetur adipisicing elit. Natus
                  in cum odio. Lorem ipsum dolor sit amet consectetur
                  adipisicing elit. Natus in cum odio. Lorem ipsum dolor sit
                  amet consectetur adipisicing elit. Natus in cum odio.
                </p>
                <p>
                  Illum repudiandae ratione facere explicabo. Consequatur dolor
                  optio iusto, quos autem voluptate ea? Sunt laudantium fugiat,
                  mollitia voluptate? Modi blanditiis veniam nesciunt architecto
                  odit voluptatum tempore impedit magnam itaque natus!
                </p>
                <p>
                  Lorem ipsum dolor sit amet consectetur adipisicing elit. Natus
                  in cum odio. Lorem ipsum dolor sit amet consectetur
                  adipisicing elit. Natus in cum odio. Lorem ipsum dolor sit
                  amet consectetur adipisicing elit. Natus in cum odio.
                </p>
                <p>
                  Illum repudiandae ratione facere explicabo. Consequatur dolor
                  optio iusto, quos autem voluptate ea? Sunt laudantium fugiat,
                  mollitia voluptate? Modi blanditiis veniam nesciunt architecto
                  odit voluptatum tempore impedit magnam itaque natus!
                </p>
                <p>
                  Lorem ipsum dolor sit amet consectetur adipisicing elit. Natus
                  in cum odio. Lorem ipsum dolor sit amet consectetur
                  adipisicing elit. Natus in cum odio. Lorem ipsum dolor sit
                  amet consectetur adipisicing elit. Natus in cum odio.
                </p>
                <p>
                  Illum repudiandae ratione facere explicabo. Consequatur dolor
                  optio iusto, quos autem voluptate ea? Sunt laudantium fugiat,
                  mollitia voluptate? Modi blanditiis veniam nesciunt architecto
                  odit voluptatum tempore impedit magnam itaque natus!
                </p>
                <p>
                  Lorem ipsum dolor sit amet consectetur adipisicing elit. Natus
                  in cum odio. Lorem ipsum dolor sit amet consectetur
                  adipisicing elit. Natus in cum odio. Lorem ipsum dolor sit
                  amet consectetur adipisicing elit. Natus in cum odio.
                </p>
                <p>
                  Illum repudiandae ratione facere explicabo. Consequatur dolor
                  optio iusto, quos autem voluptate ea? Sunt laudantium fugiat,
                  mollitia voluptate? Modi blanditiis veniam nesciunt architecto
                  odit voluptatum tempore impedit magnam itaque natus!
                </p>
              </div>
            </div>
          </div>
        </div>
      </Page>
    );
  }
}

const mapStateToProps = state => ({});

export default connect(state => ({}, mapDispatch))(Term);

const mapDispatch = dispatch => {
  const allActionProps = Object.assign({}, dispatch);
  return allActionProps;
};
