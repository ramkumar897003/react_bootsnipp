import { apiUrl } from "../constants";
import axios from "axios";
import types from "../types";
import MakeTheApiCall, { GenerateOptions } from "./ApiCalls";
import {
  addRelatedScripts,
  updateDetail,
  toggleLoading,
  getTotalPages,
  getCategories
} from "../actions/listing";
let limit = 12;

export const getList = (pageNo, keyword, category, tag) => {
  let data = {
    page: pageNo,
    keyword: keyword,
    limit: limit,
    category: category,
    tag: tag
  };
  var options = GenerateOptions("scripts", "POST", data);
  return dispatch => {
    dispatch(toggleLoading(true));
    return MakeTheApiCall(options)
      .then(response => {
        dispatch(toggleLoading(false));
        dispatch(getTotalPages(response.data.total));
        return dispatch({
          type: types.GET_SCRIPTS,
          payload: response.data.scripts
        });
      })
      .catch(error => {
        return error.response.data;
      });
  };
};

export const getUserScripts = (pageNo, keyword, category, tag) => {
  let data = {
    page: pageNo,
    keyword: keyword,
    limit: limit,
    category: category,
    tag: tag
  };
  var options = GenerateOptions("users/favourites", "POST", data, "user");
  return dispatch => {
    dispatch(toggleLoading(true));
    return MakeTheApiCall(options)
      .then(response => {
        dispatch(toggleLoading(false));
        dispatch(getTotalPages(response.data.total));
        return dispatch({
          type: types.GET_SCRIPTS,
          payload: response.data.scripts
        });
      })
      .catch(error => {
        return error.response.data;
      });
  };
};

export const getDetail = id => {
  var options = GenerateOptions("scripts/" + id, "GET");
  return dispatch => {
    return MakeTheApiCall(options)
      .then(response => {
        return dispatch(updateDetail(response.data.script));
      })
      .catch(error => {
        return error.response.data;
      });
  };
};

export const getRelatedScripts = id => {
  let data = { script_id: id };
  var options = GenerateOptions("scripts/related", "POST", data);
  return dispatch => {
    return MakeTheApiCall(options)
      .then(response => {
        return dispatch(addRelatedScripts(response.data.scripts));
      })
      .catch(error => {
        return error.response.data;
      });
  };
};

export const getAllCategories = (pageNo, limit) => {
  let data = { page: pageNo, limit: limit };
  var options = GenerateOptions("categories", "POST", data);
  return dispatch => {
    return MakeTheApiCall(options)
      .then(response => {
        dispatch(getCategories(response.data.categories));
        return response.data.categories;
      })
      .catch(error => {
        return error.response.data;
      });
  };
};
