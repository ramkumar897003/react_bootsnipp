import MakeTheApiCall, { GenerateOptions } from "./ApiCalls";
import { saveUserAuth, togglefavScript, addFavScripts } from "../actions/auth";

export const contactus = data => {
  var options = GenerateOptions("contact", "POST", data, "user");
  return dispatch => {
    return MakeTheApiCall(options)
      .then(response => {
        return response.data.message;
      })
      .catch(error => {
        return error.response.data;
      });
  };
};

export const signupApi = data => {
  var options = GenerateOptions("users/signup", "POST", data, "user");
  return dispatch => {
    return MakeTheApiCall(options)
      .then(response => {
        return response.data.message;
      })
      .catch(error => {
        return error.response.data;
      });
  };
};

export const loginAPI = data => {
  var options = GenerateOptions("users/login", "POST", data, "user");
  return dispatch => {
    return MakeTheApiCall(options)
      .then(response => {
        let userData = { email: data.email, token: response.data.token };
        localStorage.setItem("UserData", JSON.stringify(userData));
        localStorage.setItem("User_token", response.data.token);
        dispatch(saveUserAuth(userData));
        return response.data;
      })
      .catch(error => {
        console.log(error.response.data, "error");
        return error.response.data;
      });
  };
};

export const verifyAccount = token => {
  var options = GenerateOptions(
    "users/verifyaccount?token=" + token,
    "GET",
    "",
    "user"
  );
  return dispatch => {
    return MakeTheApiCall(options)
      .then(response => {
        return response.data;
      })
      .catch(error => {
        return error.response.data;
      });
  };
};

export const getUserScriptsIds = () => {
  var options = GenerateOptions("users/favouriteids", "GET", "", "user");
  return dispatch => {
    return MakeTheApiCall(options)
      .then(response => {
        dispatch(addFavScripts(response.data.scripts));
        return response.data;
      })
      .catch(error => {
        return error.response.data;
      });
  };
};

export const toggleFavScript = data => {
  var options = GenerateOptions("users/togglefavourite", "PUT", data, "user");
  return dispatch => {
    return MakeTheApiCall(options)
      .then(response => {
        dispatch(togglefavScript(data.script_id));
        return response.data;
      })
      .catch(error => {
        return error.response.data;
      });
  };
};
