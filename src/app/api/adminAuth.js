import { apiUrl } from "../constants";
import types from "../types";
import MakeTheApiCall, { GenerateOptions } from "./ApiCalls";
import {
  saveAdminScripts,
  saveNewScripts,
  saveCategory,
  saveNewCategory,
  updateCategory,
  updateScript
} from "../actions/adminAuth";

export const adminLogin = data => {
  var options = GenerateOptions("admin/login", "POST", data);
  return dispatch => {
    return MakeTheApiCall(options)
      .then(response => {
        localStorage.setItem("Admin_token", response.data.token);
        return response.data;
      })
      .catch(error => {
        return error.response.data;
      });
  };
};

export const getAdminScripts = page => {
  var options = GenerateOptions("admin/scripts", "GET", { page });
  return dispatch => {
    return MakeTheApiCall(options)
      .then(response => {
        dispatch(saveAdminScripts(response.data));
        return response.data;
      })
      .catch(error => {
        return error.response.data;
      });
  };
};

export const AddScript = data => {
  let formData = new FormData();
  Object.keys(data).forEach(key => {
    let formatedData = data[key];
    formData.append(key, formatedData);
  });
  var options = GenerateOptions("admin/scripts", "POST", data);
  return dispatch => {
    return MakeTheApiCall(options)
      .then(response => {
        dispatch(saveNewScripts(response.data.script));
        return response.data.script;
      })
      .catch(error => {
        return error.response.data;
      });
  };
};

export const UpdateScript = data => {
  var options = GenerateOptions("admin/scripts/" + data._id, "PUT", data);
  return dispatch => {
    return MakeTheApiCall(options)
      .then(response => {
        dispatch(updateScript(response.data.script));
        return response.data.script;
      })
      .catch(error => {
        return error.response.data;
      });
  };
};

export const deleteAdminScripts = id => {
  var options = GenerateOptions("admin/scripts/" + id, "DELETE");
  return dispatch => {
    return MakeTheApiCall(options)
      .then(response => {
        return response.data;
      })
      .catch(error => {
        return error.response.data;
      });
  };
};

export const getCategory = page => {
  var options = GenerateOptions("admin/categories", "GET", { page });
  return dispatch => {
    return MakeTheApiCall(options)
      .then(response => {
        dispatch(saveCategory(response.data));
        return response.data;
      })
      .catch(error => {
        return error.response.data;
      });
  };
};

export const AddCategory = data => {
  var options = GenerateOptions("admin/categories", "POST", data);
  return dispatch => {
    return MakeTheApiCall(options)
      .then(response => {
        dispatch(saveNewCategory(response.data.category));
        return response.data.category;
      })
      .catch(error => {
        return error.response.data;
      });
  };
};

export const UpdateCategory = data => {
  var options = GenerateOptions("admin/categories/" + data._id, "PUT", data);
  return dispatch => {
    return MakeTheApiCall(options)
      .then(response => {
        dispatch(updateCategory(response.data.script));
        return response.data.script;
      })
      .catch(error => {
        return error.response.data;
      });
  };
};

export const deleteCategory = id => {
  var options = GenerateOptions("admin/categories/" + id, "DELETE");
  return dispatch => {
    return MakeTheApiCall(options)
      .then(response => {
        return response.data;
      })
      .catch(error => {
        return error.response.data;
      });
  };
};
